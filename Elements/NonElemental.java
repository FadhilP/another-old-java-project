package Elements;

import Main.Element;

/**
 * NonElemental
 */
public class NonElemental extends Element {

    public NonElemental(){
        this.name = "NonElemental";
        this.affinity = 0;
    }
}