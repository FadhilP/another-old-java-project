package Races.Slimes;
import Main.*;

import java.util.Collections;
import java.util.Map;
/**
 * Slime
 */
public class Slime extends Race {

    public Slime(double health, double attack){
        
        Collections.addAll(this.SPECIES_ARCHIVE,
            new SlimeNormal(
                "",
                health
            ),

            new SlimeFire(
                "Fire Slime",
                health + 5,
                attack,
                Map.ofEntries(
                    Map.entry(elements.nonElemental, 0.5),
                    Map.entry(elements.fire, 0.5))
            )
        );
    }

    public Slime(){}

    public String toString(){
        if(this.speciesName.length() != 0)
            return this.speciesName;
        return "Slime";
    }
}