package Races.Slimes;

import java.util.Map;

import Main.*;
/**
 * SlimeFire
 */
public class SlimeFire extends Slime implements Species{

    Map<Element, Double> type;

    public SlimeFire(String speciesName, double health, double attack, Map<Element, Double> type){
        this.speciesName = speciesName;
        this.maxHealth = health;
        this.health = health;
        this.elements.fire.affinity = 0.2;
        this.attack = attack;
        this.type = type;
    }
    
}