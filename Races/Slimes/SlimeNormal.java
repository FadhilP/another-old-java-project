package Races.Slimes;
import Main.*;
/**
 * SlimeNormal
 */
public class SlimeNormal extends Slime implements Species{

    public SlimeNormal(String speciesName, double health){
        this.maxHealth = health;
        this.health = health;
    }
}