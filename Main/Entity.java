package Main;

import java.util.HashMap;
import java.util.Random;

import Races.Slimes.*;

/**
 * Entity
 */
public class Entity {

    public static Race[] RACE_ARCHIVE = {
        new Slime(5, 2),
        new Slime(5, 3)
    };

    public HashMap<String, Integer> statuses;

    public Species species;

    public Entity(){

        statuses = new HashMap<String, Integer>();

        Race race = RACE_ARCHIVE[new Random().nextInt(RACE_ARCHIVE.length)];
        this.species = race.SPECIES_ARCHIVE.get(new Random().nextInt(race.SPECIES_ARCHIVE.size()));
    }

    public void Update(){
        for(String key : statuses.keySet()){
            Status.Update(this, key);
        }
    }

}