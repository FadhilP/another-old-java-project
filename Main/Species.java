package Main;

import java.util.Map;

/**
 * Species
 */
public interface Species extends RaceInterface{

    public void Attack(Entity other, Map<Element, Double> type);

    public double GetHealth();
    public void SetHealth(double health);

    public Element GetElement();
}