package Main;

/**
 * Status
 */
public class Status {

    public static void Burn(Entity entity, int duration){

        if(duration == 0) 
            return;
    
        Put(entity, "Burn", duration);

        entity.species.SetHealth(entity.species.GetHealth() - entity.species.GetHealth() * (0.01 * duration));
    }

    public static void Put(Entity entity, String key, int duration){
        if(entity.statuses.containsKey(key)){
            if(entity.statuses.get(key) + duration == 0) 
                return; 
            entity.statuses.put(key, (entity.statuses.get(key) + duration) < 9 ? (entity.statuses.get(key) + duration) : 9);
        }
        else 
            entity.statuses.put(key, duration < 9 ? duration : 9);
    }

    public static void Update(Entity entity, String key){
        if(key.equals("Burn")) Burn(entity, -1);
    }
}