package Main;

import java.util.ArrayList;
import java.util.Map;

/**
 * RaceClass
 */
public class Race implements RaceInterface{
    
    public ArrayList<Species> SPECIES_ARCHIVE = new ArrayList<Species>();
    
    public String speciesName;

    public double maxHealth;
    public double health;

    public double attack;

    public Element elements = new Element(1);

    public void Attack(Entity other, Map<Element, Double> type){
        int count = 0;
        for(Element key : type.keySet()){
            if(elements.allElements[count].name.equals(key.name)){
                other.species.SetHealth(other.species.GetHealth() - (this.attack * type.get(key) * (1 - elements.allElements[count].affinity)));
                key.Status(other);
            }
            count++;
        }
    }

    public double GetHealth(){
        return this.health;
    }

    public void SetHealth(double health){
        this.health = health;
    }

    public Element GetElement(){
        return this.elements;
    }
}