package Main;
import Elements.NonElemental;
import Elements.Fire.*;
/**
 * Elements
 */
public class Element {

    public String name;
    public double affinity;

    public NonElemental nonElemental;
    public Fire fire;

    public Element[] allElements = {
        nonElemental,
        fire
    };

    public Element(int PH){
        this.nonElemental= new NonElemental();
        this.fire = new Fire(0);
    }

    public Element(){}

    public void Status(Entity entity){}

    public String toString(){
        return name;
    }
}